import os
from pymongo import MongoClient

data = [
	{
		'name': 'Las cronicas de Narnia',
		'totalPages': 2
	},
	{
		'name': 'Harry Potter',
		'totalPages': 3
	}
]


def generate_text(name, page):
    lorem = ("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu velit imperdiet lacus posuere finibus. Proin pharetra consectetur varius. Ut auctor nibh nec nisl consequat, sit amet mattis lectus suscipit. Nullam lacinia porttitor lectus, eget malesuada lorem volutpat sit amet. Aliquam erat volutpat. Sed imperdiet mi metus. Maecenas porta imperdiet sem. Etiam mattis fringilla orci, vel vulputate velit varius ut. Suspendisse luctus mauris sit amet eleifend aliquam. Praesent tincidunt felis mauris, eget fermentum lectus sagittis auctor. Cras faucibus congue elit eu sollicitudin. Nam nec nisi id dolor convallis sodales vehicula nec justo. Curabitur in lacus sit amet augue lobortis pulvinar sed quis diam. Pellentesque eros odio, rhoncus a lacinia eget, rhoncus non sapien."

	"In hac habitasse platea dictumst. Curabitur vel bibendum orci, sed sodales erat. Vestibulum et urna id arcu pulvinar efficitur. Nulla in odio semper odio pellentesque scelerisque id ut libero. Mauris quam purus, suscipit eu felis vel, placerat lobortis urna. Morbi a ligula orci. Fusce eleifend dui mi, eget facilisis lectus sollicitudin ac. Ut et consectetur eros. Nam faucibus volutpat nibh. Vivamus viverra tristique justo, in mattis nibh gravida a. Nulla vitae lacinia nisi. Curabitur consequat turpis ante, sed ullamcorper tellus commodo et. Vivamus non condimentum eros.")
    return f"Pagina {page} del libro {name}{lorem}"

def generate_html(name, page):
    lorem = 'lorem ipsum'
    return f"<h1>Pagina {page} del libro {name}</h1><br/><p>{lorem}</p>"

def create_folders(new_product):
    try: 
        os.mkdir(os.path.join(os.path.dirname(__file__), f"..\\api\\books\\{new_product['_id']}"))
    except OSError as error: 
        print(error)

def create_files(new_product):
    i = 1
    while i <= new_product['totalPages']:
        f = open(os.path.join(os.path.dirname(__file__), f"..\\api\\books\\{new_product['_id']}\\{i}.txt"), "w+")
        f.write(generate_text(new_product['name'], i))
        f.close()

        f = open(os.path.join(os.path.dirname(__file__), f"..\\api\\books\\{new_product['_id']}\\{i}.html"), "w+")
        f.write(generate_html(new_product['name'], i))
        f.close()
        i += 1

def run():
    client = MongoClient()
    db = client['libraryDB']
    for book in data:
        new_book = db.api_book.insert_one(book)
        book.update({'_id': str(new_book.inserted_id)})
        create_folders(book)
        create_files(book)
    