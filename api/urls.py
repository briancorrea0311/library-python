from django.urls import path
from api import views

urlpatterns = [
    path('', views.home),
    path('book', views.book_list),
    path('book/<_id>', views.book_detail),
    path('book/<_id>/page/<page>', views.page_detail),
    path('book/<_id>/page/<page>/<page_format>', views.page_detail),
]
