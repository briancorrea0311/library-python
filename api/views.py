import os
import sys
from bson import ObjectId
from rest_framework import status, decorators
from django.http.response import JsonResponse, HttpResponse
from django.shortcuts import render
from api.models import Book
from api.serializers import BookSerializer

def home(request):
    return render(request, 'index.html')

@decorators.api_view(['GET'])
def book_list(request):
    try:
        books = Book.objects.all()
        book_serializer = BookSerializer(books, many=True)
        return JsonResponse({'books': book_serializer.data}, safe=False)
    except:
        return JsonResponse(sys.exc_info()[0])

@decorators.api_view(['GET'])
def book_detail(request, _id):
    try:
        book = Book.objects.get(pk=ObjectId(_id))
        book_serializer = BookSerializer(book)
        return JsonResponse(book_serializer.data)
    except Book.DoesNotExist:
        return JsonResponse({'message': 'Ese libro no existe'}, status=status.HTTP_404_NOT_FOUND)
    except:
        return JsonResponse(sys.exc_info()[0])


@decorators.api_view(['GET'])
def page_detail(request, _id, page, page_format=None):
    try:
        file_name = ""
        if page_format is not None and page_format == "html":
            file_name = f"{page}.html"
        else:
            file_name = f"{page}.txt"
        data = open(os.path.join(os.path.dirname(__file__), f"books\\{_id}\\{file_name}"), "r")
        contents = data.read()
        return HttpResponse(contents)
    except:
        return JsonResponse(sys.exc_info()[0])
